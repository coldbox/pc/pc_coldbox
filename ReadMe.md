# Coldbox/Tessie Python Library via MQTT

This Python library allows you to control the Coldbox/Tessie system via the MQTT protocol.

## Installation

Ensure you have the `paho-mqtt` library installed:

```bash
pip install paho-mqtt
```

## Usage

You can either:

1. **Import the library into another script** and use it programmatically.
2. **Use it interactively** in a Python shell.

Example interactive usage:

```python
python
from coldbox import *

box = Coldbox('your_mqtt_broker_address')
print(box.tecall.getTemp())  # Retrieve all temperatures
```

---

# Classes Overview

## Tessie

The `Tessie` class facilitates communication with the MQTT broker and provides methods to interact with the system.

### Attributes
- `broker` (str): MQTT broker address.
- `port` (int): MQTT broker port (default is 1883).
- `topic` (str): MQTT topic to subscribe to.
- `_client` (`mqtt_client.Client`): Paho MQTT client instance.
- `waiting` (list): Variables awaiting responses.
- `found` (list): List of received variables and their corresponding values.

### Methods
- `__init__(broker)`: Initializes the MQTT client, connects to the broker, and subscribes to the topic.
- `on_connect(client, userdata, flags, rc)`: Callback for successful broker connection.
- `_connect_mqtt()`: Establishes connection to the MQTT broker.
- `decode_msg(msg)`: Decodes and processes received MQTT messages.
- `on_message(client, userdata, msg_recv)`: Callback for handling received MQTT messages.
- `_subscribe()`: Subscribes to the MQTT topic.
- `_wait_for_var(var)`: Waits for a specific variable's response.
- `get(var, args="")`: Retrieves a variable's value.
- `set(var, data, args="")`: Sets a variable's value.
- `cmd(cmd, args="", answer=False)`: Sends a command.
- `help()`: Sends a help request.

---

## Valve

The `Valve` class represents a controllable valve.

### Methods
- `__init__(tessie, i)`: Initializes the valve with a Tessie instance and index.
- `set(value)`: Sets the valve to "on" (1) or "off" (0).
- `get()`: Retrieves the current state (1 for "on", 0 for "off").

---

## Env (Environment)

The `Env` class provides access to environmental data.

### Methods
- `__init__(tessie)`: Initializes the environment instance.
- `getRH()`: Returns relative humidity as a float.
- `getDP()`: Returns dew point as a float.
- `getTempAir()`: Returns air temperature as a float.
- `getTempWater()`: Returns water temperature as a float.
- `getVprobe(number)`: Returns voltage probe data as a list of floats.

---

## TEC (Thermoelectric Cooler)

The `TEC` class represents a TEC device with operational and configuration methods.

### Methods
- `__init__(tessie, i)`: Initializes the TEC instance.
- `pon()`: Powers on the TEC.
- `poff()`: Powers off the TEC.
- `getState()`: Returns power state as a list of booleans.
- `getTemp()`: Retrieves the TEC temperature.
- `getUI()`: Retrieves voltage and current data.
- `setTemp(temp)`: Sets the TEC's target temperature.
- `setVoltage(u)`: Sets the control voltage.
- `getVoltage()`: Retrieves the control voltage.
- `reset()`: Reboots the TEC.
- `loadFromFlash()`: Loads stored variables from flash memory.
- `getSWVersion()`: Retrieves the software version.

---

## ConfTEC

The `ConfTEC` class handles TEC configuration settings.

### Methods
- `__init__(tessie, i)`: Initializes the TEC configuration.
- `saveToFlash()`: Saves configuration to flash memory.
- `getPID()`: Retrieves PID control constants `[kp, ki, kd]`.
- `setPID(kp, ki, kd)`: Sets PID control constants.
- `getPIDMinMax()`: Retrieves PID output range `[min, max]`.
- `setPIDMinMax(min, max)`: Sets PID output range.
- `setRef(ref)`: Sets reference temperature.
- `getRef()`: Retrieves reference temperature.
- `setMode(mode)`: Sets TEC operating mode.
- `getMode()`: Retrieves TEC operating mode.
- `clearError()`: Clears TEC errors.
- `getError()`: Retrieves TEC error codes.

---

## Coldbox

The `Coldbox` class aggregates multiple TECs, valves, and environmental sensors into a single interface.

### Attributes
- `valve0` / `valve1`: Valve instances.
- `tecall`: Master TEC instance for all TECs.
- `tec1`-`tec8`: Individual TEC instances.
- `env`: Environmental sensor instance.

### Methods
- `__init__(broker)`: Initializes the Coldbox with a specified MQTT broker.
- `help()`: Sends a help request to the Tessie system.

---

# Example Code

```python
def main():
    # Initialize Coldbox with broker address
    box = Coldbox('coldbox02.psi.ch')

    # Get temperature from TEC 1
    print(f"TEC1 Temperature: {box.tec1.getTemp()} °C")

    # Turn on TEC 2
    box.tec2.pon()

    # Set TEC 3 target temperature
    box.tec3.setTemp(25.0)

if __name__ == "__main__":
    main()
```

---
